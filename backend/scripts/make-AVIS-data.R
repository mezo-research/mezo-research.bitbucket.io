# ---------------------------------------------------------------------
# R script to process Victorian Rock Lobster .RData files:
# - script produces AVIS-compatible input data files
# 
# Developed by Mezo Research for Victorian Fisheries Authority.
# ---------------------------------------------------------------------

# Load required libraries:
library(dplyr)
library(stringr)
library(tidyr)
library(readr)
library(reshape2)

# Specify the location of the input data:
data_dir_in <- "./backend/data"

# Specify the location of the output data:
data_dir_out <- "./frontend/assets/data"

# Load the existing RData objects:
load(file.path(data_dir_in, "catch_effort.RData"))
load(file.path(data_dir_in, "cpue.RData"))
load(file.path(data_dir_in, "length_freq.RData"))

# Load melba output files, and set as east/west objects:
load(file.path(data_dir_in, "melba_east.RData"))
report_east <- report
rm(report)

load(file.path(data_dir_in, "melba_west.RData"))
report_west <- report_east
rm(report)

# Set model output years for plotting in AVIS:
model_output_years <- 1978:2017

# ---------------------------------------------------------------------
# Construct the ZONE_ce_summary.csv files
# ---------------------------------------------------------------------
ce_object <- left_join(ce_summaries, cpue_summary, by = c("f_season", "zone", "region")) %>%
  select(f_season,
         zone,
         region,
         catch_kg = cor_kg,
         catch_num = cor_num,
         effort_pots = cor_pot,
         cpue_nom = raw_cpue,
         cpue_stan = cpue_predict) %>%
  mutate(cpue_nom = round(cpue_nom, digits = 2),
         cpue_stan = round(cpue_stan, digits = 2))

# Select and export the EAST data to file:
ce_object_east <- filter(ce_object, zone=="East") %>% select(-zone)
  
write.csv(ce_object_east, file.path(data_dir_out, "EAST_ce_summary.csv"), quote = FALSE, row.names = FALSE)

# Select and export the WEST data to file:
ce_object_west <- filter(ce_object, zone=="West") %>% select(-zone)
  
write.csv(ce_object_west, file.path(data_dir_out, "WEST_ce_summary.csv"), quote = FALSE, row.names = FALSE)


# ---------------------------------------------------------------------
# Construct the ZONE_FL.csv files
# ---------------------------------------------------------------------
# Start with necessary mutations for incoming data
# add f_season column to sizes_fyear and effort:
sizes_fyear <- sizes_fyear %>%
  mutate(f_season = paste0(as.character(FYear),"/",str_sub(as.character(FYear+1), -2)))
effort <- effort %>%
  mutate(f_season = paste0(as.character(FYear),"/",str_sub(as.character(FYear+1), -2)))

# Remove data without a recorded sex:
sizes_fyear <- filter(sizes_fyear, Sex %in% c('M', 'F'))

# Make sure all length data is to the nearest mm (rounded with no decimal places):
sizes_fyear$Length <- round(sizes_fyear$Length)

# Set the minimum and maximum bin values:
floor_bin <- 0       # lowest possible length
min_bin <- 40        # upper edge of the smallest bin
max_bin <- 210       # lower edge of the largest bin
ceiling_bin <- 300   # largest possible length
width <- 1           # width of bins between min_bin and max_bin

# Create a table that matches all possible lengths to a specific bin:
# - calculate all possible bins: [floor_bin:min_bin], (min_bin:width:max_bin], (max_bin:ceiling_bin]
# - bins are labeled according to the maximum value that appears in that bin, execpt in the case of the upper bin,
# which is labeled according to the minimum value that appears in that bin
length_to_bin <- tibble(length = c(floor_bin:ceiling_bin)) %>%
  mutate(bin_number = cut(length, breaks = c(floor_bin, seq(min_bin, max_bin-width, width), ceiling_bin), 
                   include.lowest = TRUE, label = FALSE)) %>%
  mutate(bin = min_bin + (bin_number - 1)*width) %>%
  select(-bin_number)

# Create a tibble with every bin size -- include Sex for match to unique tibble below:
bin_tibble_F <- length_to_bin %>% mutate(Sex = "F")
bin_tibble_M <- length_to_bin %>% mutate(Sex = "M")
bin_tibble <- rbind(bin_tibble_F, bin_tibble_M)

# Identify every unique combination of Zone, Type, f_season and Sex:
sizes_fyear_unique <- sizes_fyear %>%
  select(Zone, f_season, Type, Sex) %>%
  group_by(Zone, f_season, Sex, Type) %>%
  distinct() %>%
  left_join(bin_tibble, by = "Sex")

# Count number of entries in each length bin:
# - if below the min length, change the length to the minimum length bin value
# - if above the max length, change the length to the maximum length bin value
sizes_fyear_count <- sizes_fyear %>%
  mutate(Length = case_when(Length <= min_bin ~ min_bin,
                            Length >= max_bin ~ max_bin,
                            TRUE ~ Length)) %>%
  group_by(Zone, f_season, Sex, Type, Length) %>%
  summarise(count = n())

# Join the count to the list of unique entries and replace NA with zero:
sizes_fyear_lenbins <- sizes_fyear_unique %>%
  left_join(sizes_fyear_count, by = c("Zone", "f_season", "Type", "Sex", "length" = "Length")) %>%
  replace_na(list(count = 0)) 

# Remove length and sort based on existing output csv structure:
sizes_fyear_bins <- sizes_fyear_lenbins %>%
  select(-length) %>%
  group_by(Zone, f_season, Sex, Type, bin) %>%
  summarise_at(vars(count), sum) %>%
  arrange(Zone, bin, Sex, Type, desc(f_season))

# Construct the complete object:
lf_object <- left_join(sizes_fyear_bins, effort, by = c("Zone", "f_season", "Type")) %>%
  ungroup() %>%
  select(Zone, 
         f_season,
         survey_type = Type,
         sex = Sex,
         bin,
         count,
         effort = Pothauls)

# Select and export the EAST file:
lf_object_east <- filter(lf_object, Zone=="East") %>% select(-Zone)

write.csv(lf_object_east, file.path(data_dir_out, "EAST_LF.csv"), quote = FALSE, row.names = FALSE)

# Select and export the WEST file
lf_object_west <- filter(lf_object, Zone=="West") %>% select(-Zone)

write.csv(lf_object_west, file.path(data_dir_out, "WEST_LF.csv"), quote = FALSE, row.names = FALSE)


# ---------------------------------------------------------------------
# Construct the ZONE_survey.csv files
# ---------------------------------------------------------------------
# This object also requires count and effort, so use sizes_fyear_allbins as a starting point,
# then join effort at appropriate summary level:

# Secify the length ranges:
F_length_bound <- 105
M_length_bound <- 110

# Update lf_object to have a new column "group" -- distinguishes whether 'Size' or 'Undersize' 
# based on Sex and Length:
sizes_fyear_grouped <- sizes_fyear_lenbins %>%
  mutate(group = case_when(Sex == "F" & length < F_length_bound ~ "Undersize",
                           Sex == "F" & length >= F_length_bound ~ "Size",
                           Sex == "M" & length < M_length_bound ~ "Undersize",
                           Sex == "M" & length >= M_length_bound ~ "Size",
                           TRUE ~ "None")) %>%
  group_by(Zone, f_season, Sex, Type, group) %>%
  summarise_at(vars(count), sum)

# Select and rename columns for output:  
survey_object <- left_join(sizes_fyear_grouped, effort, by = c("Zone", "f_season", "Type")) %>%
  ungroup() %>%
  select(Zone, 
         f_season,
         survey_type = Type,
         sex = Sex,
         group,
         effort = Pothauls,
         count)
  
# Select and export the EAST file:
survey_object_east <- filter(survey_object, Zone=="East") %>% select(-Zone)
  
write.csv(survey_object_east, file.path(data_dir_out, "EAST_survey.csv"), quote = FALSE, row.names = FALSE)

# Select and export the WEST file:
survey_object_west <- filter(survey_object, Zone=="West") %>% select(-Zone)

write.csv(survey_object_west, file.path(data_dir_out, "WEST_survey.csv"), quote = FALSE, row.names = FALSE)

# ---------------------------------------------------------------------
# Construct the ZONE_json_input.csv files
# ---------------------------------------------------------------------

# Select and rename required columns from catch and effort data frame (ce_object):
map_select <- select(ce_object, zone, 
                     year = f_season,
                     name = region, 
                     catch = catch_kg, 
                     effort = effort_pots, 
                     cpue = cpue_stan) 

# Create or modify columns with rounded numbers, simple years, and geomfile names:
map_object <- mutate(map_select, 
                     year = as.numeric(substr(year, 1, 4)),
                     id = 0,
                     catch = round(catch/1000, 1),
                     effort = round(effort/1000, 1),
                     cpue = round(cpue, 2),
                     geomfile = paste0(gsub(" ", "", tolower(name)), '.txt')) %>%
  
  # Filter for most recent year, and remove 'Whole zone' entries:
  filter(year == max(year)) %>% filter(name != 'Whole zone')

# Assign map plot id numbers to each known area for AVIS code:
map_object$id[which(map_object$name == 'Queenscliff')]    <- 1
map_object$id[which(map_object$name == 'San Remo')]       <- 2
map_object$id[which(map_object$name == 'Lakes Entrance')] <- 3 

map_object$id[which(map_object$name == 'Portland')]    <- 1
map_object$id[which(map_object$name == 'Warrnambool')] <- 2
map_object$id[which(map_object$name == 'Apollo Bay')]  <- 3 

# Construct and export the EAST json input file:
map_object_east <- filter(map_object, zone == "East") %>% select(-zone, -year)

write.csv(map_object_east, file.path(data_dir_out,"EAST_json_input.csv"), quote = FALSE, row.names = FALSE)

# Construct and export the WEST json input file:
map_object_west <- filter(map_object, zone == "West") %>% select(-zone, -year)

write.csv(map_object_west, file.path(data_dir_out,"WEST_json_input.csv"), quote = FALSE, row.names = FALSE)

# ---------------------------------------------------------------------
# Construct the ZONE_region.js files
# ---------------------------------------------------------------------
# Begin by defining the function used to generate the ZONE_region.js file
# This takes ZONE_json_input.csv and returns a vector of strings for the ZONE_region.js file
# ZONE_json_input.csv should have columns: id,name,catch,effort,cpue,geomfile
get_js_string <- function(inputfile){
  
  # Define the strings that need to be inserted into the file:
  opening = 'var regionsData = {"type": "FeatureCollection", "features": ['
  closing = ']};'
  
  # Read the input csv:
  input_table <- read.csv(inputfile, colClasses=rep("character", 6))
  
  # Get the number of regions to be looped over:
  nregions <- nrow(input_table)
  
  # Construct the vector that will be used to construct the output string:
  # - there is a starting and closing entry, plus one for each region, making the length nreg+2
  output <- vector(mode="character", length=(nregions+2))
  
  # Add the opening string to the output vector:
  output[1] <- opening
  
  # Loop over regions listed in the input file:
  for (row in 1:nregions){
    # Get the geometry string from the existing text file
    geom_string <- read_lines(file.path(data_dir_out,"geometry",input_table[row,'geomfile']))
    
    # Construct the full string for each region
    feature_string <- sprintf('{ "type": "Feature", "id": "%s", "properties": { "name": "%s", "catch": %s, "effort": %s, "cpue": %s }, %s }', 
                              input_table[row,'id'], input_table[row,'name'], input_table[row,'catch'],
                              input_table[row,'effort'], input_table[row,'cpue'], geom_string)
    
    # If not the last region, append a comma
    if (row < nregions){
      output[row+1] <- paste0(feature_string, ",")
    }
    else {
      output[row+1] <- paste0(feature_string)
    }
    
  }
  # Add the closing string to the output vector
  output[(nregions+2)] <- closing
  
  return(output)
}

# Construct and export the EAST json file:
json_input_east <- file.path(data_dir_out,"EAST_json_input.csv")
east_output <- get_js_string(json_input_east)
writeLines(east_output, file.path(data_dir_out, "EAST_regions.js"))

# Construct and export the WEST json file:
json_input_west <- file.path(data_dir_out,"WEST_json_input.csv")
west_output <- get_js_string(json_input_west)
writeLines(west_output, file.path(data_dir_out, "WEST_regions.js"))


#--------------------------------------------
# Construct model output files
#--------------------------------------------

###########
# West Zone
###########

# Get exploitable biomass as a dataframe and fix column names:
exp_avg_legal <- as.data.frame(report_west$ExploitAvgYr_TotLegal)
colnames(exp_avg_legal) <- c("Commercial", "Recreational")

# Add year column and filter for model range:
exp_avg_legal <- exp_avg_legal %>%
  mutate(Year = as.numeric(report_west$year_init : (report_west$year_max + report_west$Nproj))) %>%
  filter(Year %in% model_output_years) %>%
  select(Year, biomass = Commercial)

# Get harvest rate as a dataframe and fix column names:
hrate_total_legal_com <- as.data.frame(report_west$HrateYr_TotLegal_Com[1:length(model_output_years),1])
colnames(hrate_total_legal_com) <- c("exploitation")

# Add year column and filter for model range:
hrate_total_legal_com <- hrate_total_legal_com %>%
  mutate(Year = model_output_years) %>%
  select(Year, exploitation)

# Get egg production as a dataframe and fix column names:
egg_production <- as.data.frame(report_west$EggProd)
colnames(egg_production) <- c("Egg_Production")

# Add year column and filter for model range:
egg_production <- egg_production %>%
  mutate(Year = as.numeric(report_west$year_init : (report_west$year_max + report_west$Nproj))) %>%
  filter(Year %in% model_output_years) %>%
  select(Year, Egg_Production)

# Egg production is anchored on 2001 which is set at 57% for WZ and 19.2% for EZ (units are % of 1951 levels):
egg_prod_standard <- filter(egg_production, Year == 2001)$Egg_Production / 0.57
egg_production <- egg_production %>%
  mutate(egg_prod = (Egg_Production / egg_prod_standard) * 100) %>% # make it a percentage
  select(Year, egg_prod)

# Get recruitment as a dataframe and fix column names:
recruitment <- as.data.frame(report_west$recs[1:length(report_west$recs)])
colnames(recruitment) <- c("recruitment")

# Add year column and filter for model range:
recruitment <- recruitment %>%
  mutate(Year = as.numeric(report_west$year_init : ((report_west$year_max + 1 + report_west$Nproj)))) %>%  # note that recruitment goes to year_max + 1
  filter(Year %in% model_output_years) %>%
  select(Year, recruitment)
# normalise recruitment to average:
recruitment <- recruitment %>%
  mutate(recruitment = recruitment / mean(recruitment))

# Stitch the data together:
model_output <- exp_avg_legal %>%
  left_join(hrate_total_legal_com) %>%
  left_join(recruitment) %>%
  left_join(egg_production)

# Generate display_year:
model_output <- model_output %>%
  mutate(Year2 = Year + 1) %>%
  mutate(display_year = paste0(as.character(Year), "/", substring(as.character(Year2), 3))) %>%
  select(display_year, biomass, exploitation, recruitment, egg_prod)

model_output <- model_output %>%
  melt(id.vars = "display_year", variable.name = "survey_id") %>%
  mutate(region = "All") %>%
  select(display_year, survey_id, region, value)

# Customise filename as required:
write.csv(model_output, file = file.path(data_dir_out, "WEST_model_outputs.csv"), row.names = FALSE)


###########
# East Zone
###########

# Get exploitable biomass as a dataframe and fix column names:
exp_avg_legal <- as.data.frame(report_east$ExploitAvgYr_TotLegal)
colnames(exp_avg_legal) <- c("Commercial", "Recreational")

# Add year column and filter for model range:
exp_avg_legal <- exp_avg_legal %>%
  mutate(Year = as.numeric(report_east$year_init : (report_west$year_max + report_east$Nproj))) %>%
  filter(Year %in% model_output_years) %>%
  select(Year, biomass = Commercial)

# Get harvest rate as a dataframe and fix column names:
hrate_total_legal_com <- as.data.frame(report_east$HrateYr_TotLegal_Com[1:length(model_output_years),1])
colnames(hrate_total_legal_com) <- c("exploitation")

# Add year column and filter for model range:
hrate_total_legal_com <- hrate_total_legal_com %>%
  mutate(Year = model_output_years) %>%
  select(Year, exploitation)

# Get egg production as a dataframe and fix column names:
egg_production <- as.data.frame(report_east$EggProd)
colnames(egg_production) <- c("Egg_Production")

# Add year column and filter for model range:
egg_production <- egg_production %>%
  mutate(Year = as.numeric(report_east$year_init : (report_west$year_max + report_east$Nproj))) %>%
  filter(Year %in% model_output_years) %>%
  select(Year, Egg_Production)

# Egg production is anchored on 2001 which is set at 57% for WZ and 19.2% for EZ (units are % of 1951 levels):
egg_prod_standard <- filter(egg_production, Year == 2001)$Egg_Production / 0.192
egg_production <- egg_production %>%
  mutate(egg_prod = (Egg_Production / egg_prod_standard) * 100) %>% # make it a percentage
  select(Year, egg_prod)

# Get recruitment as a dataframe and fix column names:
recruitment <- as.data.frame(report_east$recs[1:length(report_east$recs)])
colnames(recruitment) <- c("recruitment")

# Add year column and filter for model range:
recruitment <- recruitment %>%
  mutate(Year = as.numeric(report_east$year_init : (report_west$year_max + 1 +report_east$Nproj))) %>%  # note that recruitment goes to year_max + 1
  filter(Year %in% model_output_years) %>%
  select(Year, recruitment)
# normalise recruitment to average:
recruitment <- recruitment %>%
  mutate(recruitment = recruitment / mean(recruitment))

# Stitch the data together:
model_output <- exp_avg_legal %>%
  left_join(hrate_total_legal_com) %>%
  left_join(recruitment) %>%
  left_join(egg_production)

# Generate display_year:
model_output <- model_output %>%
  mutate(Year2 = Year + 1) %>%
  mutate(display_year = paste0(as.character(Year), "/", substring(as.character(Year2), 3))) %>%
  select(display_year, biomass, exploitation, recruitment, egg_prod)

model_output <- model_output %>%
  melt(id.vars = "display_year", variable.name = "survey_id") %>%
  mutate(region = "All") %>%
  select(display_year, survey_id, region, value)

# Customise filename as required:
write.csv(model_output, file = file.path(data_dir_out, "EAST_model_outputs.csv"), row.names = FALSE)


#----------------
# End of File
#----------------